#use the latest Tomcat image
FROM tomcat

#add a maintainer line with your name
MAINTAINER Lina Pupeikiene

#update the image
RUN apt-get update && apt-get clean

#clone the project
copy target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/scalatra-maven-prototype.war

expose 8080  


# the command to run when the container starts 
# there is a maven jetty plugin we can run to start the
# webservice
CMD ["catalina.sh", "run"]
